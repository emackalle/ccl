var control = {};
control.init = function(){
	$("#left_menu").html(ui.load_menu());
	control.left_menu_nav();
	$("#left_menu > li:first-child").click();
}
control.left_menu_nav = function(){
	$("#left_menu > li").unbind("click").bind("click", function(e){
		$("#left_menu > li.active").removeClass("active");
		$(this).addClass("active");
		if( $(this).find(".submenu")[0] ){
			if($(this).find(".submenu > li.active")[0]){
			}else{
				$("#left_menu .submenu").addClass("active").slideDown(200);			
				control.left_submenu_nav();
				$(this).find(".submenu > li")[0].click();				
			}
		}else{
			$("#left_menu .submenu").removeClass("active").slideUp(200);
			$("#left_menu .submenu > li.active").removeClass("active");
			control.get_form($(this).attr("data-form"));
		}
	});
}
control.left_submenu_nav = function(){
	$("#left_menu .submenu > li").unbind("click").bind("click", function(){
		$("#left_menu .submenu > li.active").removeClass("active");
		$(this).addClass("active");
		control.get_form($(this).attr("data-form"));
	});
}
control.get_form = function(form, callback){
	app.lib.ajax({ url: "./xml/"+form+".xml", type: "GET", dataType: "html" }, function(html){
		if(callback){
			callback(html);
		}else{
			$("#menu_content").html(html);
			if(form == "records"){
				control.form_records();
			}else{
				window.sessionStorage.setItem("form", form);
				control.form();
			}
		}
	});	
}
control.datepicker_range = function(start, end, param){
	var prop = param || { start_date: {}, end_date: {} };
    start.datepicker({
        defaultDate: "+1w"
    }).on( "change", function() {
       	end.datepicker( "option", "minDate", getDate( this ) );
    });
	prop.start_date.setDate == undefined ? start.datepicker("setDate", "-1m") : start.datepicker("setDate", prop.start_date.setDate);
	prop.start_date.minDate != undefined ? start.datepicker("option", "minDate", prop.start_date.minDate) : "";
    end.datepicker({
        defaultDate: "+1w"
    }).on( "change", function() {
    	start.datepicker( "option", "maxDate", getDate( this ) );
    });
	prop.end_date.setDate == undefined ? end.datepicker("setDate", "today") : end.datepicker("setDate", prop.end_date.setDate);
	prop.end_date.minDate != undefined ? end.datepicker("option", "minDate", prop.end_date.minDate) : "";
 
    function getDate( element ) {
    	var date;
      	try {
        	date = $.datepicker.parseDate( "mm/dd/yy", element.value );
      	} catch( error ) {
       		date = null;
      	}
      	return date;
    }	
}
control.datepicker = function(input){
	input.datepicker();
	input.datepicker("setDate", "today");
}
control.get_page_start = function(page_no, page_size){
	var start;	
	if(page_no == 1){
		start = 0;
	}else{
		start = (page_no-1) * page_size;
	}
	return start;
}
control.pagination = function(current_page, page_size, total_count, form, pagination_id){
	var targetId = pagination_id || "#pagination";
	var total_page = Math.ceil((total_count*1)/(page_size*1));
	$(targetId).html(ui.pagination(current_page, total_page));
	if( total_count > 0 ) control.pagination_nav(form, targetId);
}

control.pagination_nav = function(form, targetId){
	var form = form, targetId, targetId;
	$(targetId).find("a[data-page]").unbind("click.pagination").bind("click.pagination", function(){
		if(!$(this).hasClass("disabled")){
			var page_no =	$(this).attr("data-page");
			submit(form, page_no);
		}
	});
	$(targetId).find("input[name='page_no']").unbind("keyup.pagination").bind("keyup.pagination", function(e){
		if(e.keyCode == 13){
			var page_no = $(this).val();
			var total_page = ($(targetId).find(".total_page").text()*1)
			if(control.regExp("numberOnly", page_no)){						
				if(page_no <= total_page){
					submit(form, page_no);
				}else{
					$(this).val("");
				}
			}else{
				$(this).val("");
			}
		}
	});
	var submit = function(form, page_no){	
		form.attr("data-page", page_no);
		form.find("input[name='submit']").click();
	}
}

// Records
control.form_records = function(){
	$("#form_records [name='form_type']").html(ui.load_forms());
	control.import_data();
	control.export_data();
	control.datepicker_range($("#form_records [name='start_date']"), $("#form_records [name='end_date']"));
	control.form_records_submit();
	$("#form_records [name='submit']").click();
}
control.form_records_submit = function(){
	$("#form_records [name='submit']").unbind("click").bind("click", function(){
		var form = $("#form_records");
		var start_date = form.find("[name='start_date']");
		var end_date = form.find("[name='end_date']");
		var name = form.find("[name='name']");
		var form_type = form.find("[name='form_type']");
		var form_page = form.attr("data-page");
		var page_no = form_page == "" ? 1 : form_page;
		var page_size = 10;
		var page_start = control.get_page_start(page_no, page_size);
		form.attr("data-page","");		
		var data = {};
		if(name.val() != "") $.extend(data, { $or: [{ lname: name.val().toUpperCase() }, { fname: name.val().toUpperCase() }, { mname: name.val().toUpperCase() }] });
		if(form_type.val() != "") $.extend(data, { form_type: form_type.val() });
		$.extend(data, { $and: [{date: { $gte: start_date.val() }}, {date: { $lte: end_date.val() }}] });
		app.lib.loader.show();
		db.find(data).sort({ date: -1, lname: 1 }).skip(page_start).limit(page_size).exec(function (err, list) {
			db.count(data, function (err_2, total) {
				app.lib.loader.hide();
				$("#list_records").html(ui.load_records(list, page_start+1));		
				var total = list.length == 0 ? 0 : total;
				control.pagination(page_no, page_size, total, form);		
				control.select_record();
			});			
		});

	});
}
control.select_record = function(){
	$("#list_records .item").unbind("click").bind("click", function(){
		var type = $(this).attr("data-type");
		var id = $(this).attr("data-id");
		window.sessionStorage.setItem("id", id);
		control.get_form(type);
	});
}

control.export_data = function(){
	app.lib.loader.show();
	db.find({}, function (err, list) {
		app.lib.loader.hide();
		var d = new Date();
		var date = d.getFullYear() + ((d.getMonth()+1)<10 ? "0"+(d.getMonth()+1) : (d.getMonth()+1)).toString() + (d.getDate()<10 ? "0"+d.getDate() : d.getDate()).toString();
		var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(list));	
		$(".content-header h2").after("<a href='data:" +data+ "' download='" +date+ ".json' id='export_data' class='hExpBtn grn-btn' title='Export'><i class='icon-download'></i></a>");
	});
}

control.import_data = function(){
	$(".content-header h2").after("<a href='javascript:void(0);' id='' class='hImpBtn grn-btn' title='Import'><i class='icon-plus-circle'></i><input type='file' id='import_data' /></a>");
	$("#import_data").unbind("change").bind("change", function(){
    	//Retrieve the first (and only!) File from the FileList object
    	var f = this.files[0]; 
	    if (f) {
		    var a = f.name;
		    var ext = a.split(".")[a.split(".").length - 1];
		    if(ext.toLowerCase() == "json"){
		    	var r = new FileReader();
		        r.onload = function(e) { 
		        	var contents = e.target.result;
		        	try{
		        		app.lib.loader.show();
		        		db.remove({ }, { multi: true }, function (err, numRemoved){
			        		var data = JSON.parse(contents);
							db.insert(data, function(){
				        		app.lib.loader.hide();
				        		app.lib.alert("success", "Success!");
				        		$(this).val("");
				        		$("#form_records input[name='submit']").click();
							});		        			
		        		});
		        	}
		        	catch(e){
				    	app.lib.alert("error", "Corrupted Datebase");  
		        	}
		        }
		        r.readAsText(f);
		    }else{
		    	app.lib.alert("error", "Invalid File!");  
		    }
		} else { 
		    app.lib.alert("error", "Invalid File!");
		}		
	});	
}
control.go_back_btn = function(){
	$(".content-header h2").after("<a href='javascript:void(0);' id='go_back_btn' class='hAddBtn grn-btn' title='Back'><i class='icon-reply'></i></a>");
	$("#go_back_btn").unbind("click").bind("click", function(){
		control.get_form("records");
	});
}
control.delete_btn = function(id){
	var id = id;
	$(".content-header h2").after("<a href='javascript:void(0);' id='delete_item' class='hDelBtn grn-btn' title='Delete'><i class='icon-trash'></i></a>");
	$("#delete_item").unbind("click").bind("click", function(){
		app.lib.confirm("Are you sure you want to delete?", function(ok){
			if(ok){
				app.lib.loader.show();
				db.remove({ _id: id }, {}, function (err, numRemoved) {
					app.lib.loader.hide();
					$("#go_back_btn").click();
				});				
			}
		})
	});
}

// Form
control.form= function(){
	var id = window.sessionStorage.getItem("id");
	var type = window.sessionStorage.getItem("form");
	if(id){
		window.sessionStorage.removeItem("id");
		control.form_get(id);
		control.form_update(id);
		control.go_back_btn();
		control.delete_btn(id);
	}else{
		control.datepicker($("#form_" +type+ " [name='date']"));
		control.form_submit();
		$("#form_" +type+ " [name='fname']").focus();
	}
}
control.form_get = function(id){
	var type = window.sessionStorage.getItem("form");
	db.find({ "_id": id }, function (err, data) {
		for(var i=0; i<data.length; i++){
			for(var key in data[i]){
				switch(key){
					case "gender":
						$("#form_" +type+ " [name='gender'] option[value='" +data[i][key]+ "']").prop("selected", true);
						break;
					case "check_cbc":
					case "check_urinalysis":
					case "check_fecalysis":
					case "check_hepatitis_b":
					case "check_pregnancy_test":
					case "check_others":
						$("#form_" +type+ " [name='" +key+ "']").prop("checked", true);						
						break;
					case "heart_disease_opt":
						$("#form_" +type+ " [name='" +key+ "'][value='" +data[i][key]+ "']").prop("checked", true);						
						break;
					default:
						$("#form_" +type+ " [name='" +key+ "']").val(data[i][key]);				
				}
			}
		}
	});			
}
control.form_update = function(id){
	var _id = id; 
	var type = window.sessionStorage.getItem("form");
	$("#form_"+type+" [name='submit']").unbind("click").bind("click", function(){
		var form = $("#form_"+type+"");
		var data = app.lib.serializeObject(form);
		data.date = form.find("[name='date']").val();
		data.form_type = ""+type+"";
		data.fname = data.fname.toUpperCase();
		data.mname = data.mname.toUpperCase();
		data.lname = data.lname.toUpperCase();		
		app.lib.loader.show();
		// db.update({ "_id": _id }, { $set: data }, function (err, _data) {
		db.insert(data, function (err, data) {
			app.lib.loader.hide();
			control.form_print(type, data);
		});				
	});
}
control.form_submit = function(){
	var type = window.sessionStorage.getItem("form");
	$("#form_" +type+ " [name='submit']").unbind("click").bind("click", function(){
		var form = $("#form_" +type+ "");
		var data = app.lib.serializeObject(form);
		data.date = form.find("[name='date']").val();
		data.form_type = "" +type+ "";
		data.fname = data.fname.toUpperCase();
		data.mname = data.mname.toUpperCase();
		data.lname = data.lname.toUpperCase();		
		app.lib.loader.show();
		db.insert(data, function (err, data) {
			app.lib.loader.hide();
			// form.find("[name='reset']").click();
			control.datepicker($("#form_" +type+ " [name='date']"));
			control.form_print(type, data);
		});		
	});
}

// Print
control.form_print = function(form, data){
	control.get_form("print_"+form, function(_html){
		$("#print_area").attr("src","./print.html?_="+new Date().getTime());
		$("#print_area").off("load").on("load", function(){
			$("#print_area").removeClass("hide");
			data.name = "";
			if(data.fname != "" || data.lname != ""){
				if(data.lname == ""){
					data.name = data.fname + " " + data.mname;
				}else{
					data.name = data.lname + ", " + data.fname + " " +data.mname;
				}
			}
		    $("#print_area")[0].contentWindow.postMessage(JSON.stringify({ form: form, _html: _html, data: data }), "*");			
		});
		window.addEventListener("message", function(){
			$("#print_area").addClass("hide");
		}, false);
	});
}
