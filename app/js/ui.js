var ui = {};
ui.forms = ["Urinalysis","Fecalysis","Hematology","Miscellaneous","Laboratory Results", "ECG Report", "Roentgenological Report"];
ui.load_menu = function(){
	var _html = "";
	_html += "<li data-form='records' class='chart icon-chart-bar'><a href='javascript:void(0);'>Records</a></li>";				
	_html += "<li class='curr icon-doc-text-inv'>";
		_html += "<a href='javascript:void(0);'>Forms</a>";
		_html += "<ul class='submenu submenu-list hide'>";
			var forms = ui.forms;
			for(var i=0; i<forms.length; i++){
				var form = forms[i].replace(" ", "_");
				_html += "<li data-form='" +form.toLowerCase()+ "' class='icon-minus'>" +forms[i]+ "</li>";
			}
		_html += "</ul>";
	_html += "</li>";
	return _html;	
}
ui.load_forms = function(){
	var forms = ui.forms;
	var _html = "<option value=''>All</option>";
	for(var i=0; i<forms.length; i++){
		var form = forms[i].replace(" ", "_");
		_html += "<option value='" +form.toLowerCase()+ "'>" +forms[i]+ "</option>";
	}
	return _html;
}

// Pagination
ui.pagination = function(current_page, total_page){
	var prev = current_page == 1 ? 1 : (current_page*1) - 1;
	var next = current_page == total_page ? total_page : (current_page*1) + 1;
	var _html = "";
	var type = current_page == 1 ? "disabled" : "";
	_html += "<li><a href='javascript:void(0);' data-page='1' class='" +type+ "'><i class='icon-angle-double-left grn-btn'></i></a></li>";
	_html += "<li><a href='javascript:void(0);' data-page='" +prev+ "' class='" +type+ "'><i class='icon-angle-left grn-btn'></i></a></li>";
	_html += "<li class='pagination-text'><div><input type='text' name='page_no' value='" +current_page+ "' /> of <span class='total_page'>" +total_page+ "</span></div></li>";
	var type = current_page == total_page ? "disabled" : "";
	_html += "<li><a href='javascript:void(0);' data-page='" +next+ "' class='" +type+ "'><i class='icon-angle-right grn-btn'></i></a></li>";
	_html += "<li><a href='javascript:void(0);' data-page='" +total_page+ "' class='" +type+ "'><i class='icon-angle-double-right grn-btn'></i></a></li>";
	return _html;		
}

ui.load_records = function(list, start){
	var _html = "";
	if(list.length > 0){
		for(var i=0; i<list.length; i++){
			var lname = list[i].lname || "";
			var fname = list[i].fname || "";
			var date = list[i].date || "";
			var form_type = ui.form_type(list[i].form_type);
			_html += "<tr class='item' data-type='" +list[i].form_type+ "' data-id='" +list[i]._id+ "'>";
				_html += "<td>" +(start+i)+ "</td>";
				_html += "<td>" +(lname +", "+ fname)+ "</td>";
				_html += "<td>" +date+ "</td>";
				_html += "<td>" +form_type+ "</td>";
			_html += "</tr>";
		}
	}else{
		_html += "<tr class='no-records'><td colspan='4' class='no-records'>" +app.lib.prop("no_result")+ "</td></tr>";		
	}
	return _html;
}

ui.form_type = function(form){
	var form_type = "";
	for(var i = 0; i<ui.forms.length; i++){
		if(ui.forms[i].toLowerCase() == form.replace("_"," ").toLowerCase()){
			form_type = ui.forms[i];
			break;
		}
	}
	return form_type;
}

ui.form_print = function(form){
	var _html = "";
	_html += "<img src='./img/print.jpg' />";
	return _html += form;
}