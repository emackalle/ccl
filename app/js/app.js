(function(window, document, $){
	window.app={};
	var app=window.app;
	app.global={};
	app.lib={};
	// Ajax
	app.lib.ajax=function(param, callback){
		param.type = param.type || "GET";
		param.contentType = param.contentType || "application/json";
		param.cache = param.cache || false;
		param.beforeSend = function(){
			app.lib.loader.show();
		}
		param.complete = function(rs){
			app.lib.loader.hide();
			// var response={};
			// switch(rs.status){
			// 	case 200:
			// 		response.status=true;
			// 		response.value= param.dataType == "html" ? rs.responseText : rs.responseJSON;
			// 		break;
			// 	case 500:
			// 		response.status=false;
			// 		response.errorCode="system.error";
			// 		response.value=rs.responseText;
			// 		break;
			// 	case 401:
			// 		response.status=false;
			// 		response.errorCode="login.not.session";
			// 		break;
			// 	case 400:
			// 		response.status=false;
			// 		response.errorCode=rs.responseJSON;
			// 		response.value="Request Error!";
			// 		break;
			// 	case 404:
			// 		response.status=false;
			// 		response.errorCode="system.error";
			// 		response.value="Function Not Available!";
			// 		break;
			// 	default:
			// 		console.log(rs);
			// 		break;
			// }
			var response = param.dataType == "html" ? rs.responseText : rs.responseJSON;
			callback(response)
		}
		$.ajax(param);
	};
	// Properties	
	$.i18n.browserLang=function(){ return ""; };
	$.i18n.properties({ 
		name: "messages", 
		path: "resources/", 
		mode: "map",
		lang: "en",
		cache: !0,
		callback: function() { 
		},
		error: function(){
		}
	});
	app.lib.prop=function(key){
		return $.i18n.prop(key);
	}
	// Tooltip
	app.lib.tooltip=function(targetId, message){
		$("#app_tooltip").remove();
		var targetId = targetId;
		targetId.parent().addClass("error");
		var _html = "<div id='app_tooltip' class='tooltip active'><span>" +app.lib.prop(message)+ "</span></div>";
		$(targetId).after(_html);
		$("body").unbind("click.tooltip").bind("click.tooltip", function(e){
			if( e.target.nodeName == "INPUT" && $(e.target).attr("name") == "submit" ){
			}else{
				targetId.parent().removeClass("error");
				$("#app_tooltip").remove();
				$("body").unbind("click.tooltip");
			}
		});
	}

	// Pop Message
	app.lib.message=function(type, message, wrp){
		$("#app_message").remove();
		var target = wrp ? wrp.find(".form-message") : $("body");
		var message_type = "default";
		switch(type){
			case "success":
				message_type = "success";
				break;
			case "error":
				message_type = "failed";
				break;
			case "warning":
				message_type = "warning";
				break;

		}
		var _html = "<div id='app_message' class='alert-top " +message_type+ " pop-alert'><div class='at-wrap'><p>" +app.lib.prop(message)+ "</p><span class='icon-cancel close-btn'></span></div></div>";
		target.html(_html);
		$("#app_message .close-btn").unbind("click").bind("click", function(){
			$("#app_message").remove();
		});
	}
	// Pop Alert
	app.lib.alert=function(type, message, callback){
		$("#app_alert").remove();
		var icon;
		switch(type){
			case "success":
				icon = "icon-ok-2";
				break;
			case "error":
				icon = "icon-cancel-2"
				break;
			case "warning":
				icon = "icon-attention-alt";
				break;
			default:
				icon = "";
		}
		var _html = "<div id='app_alert' class='clearfix pop-up-container v-mid'>";
		_html += "<div class='pop-alert pop-up-alert'>";
		_html += "<span class='iconStat " +icon+ "'></span>";
		_html += "<p>" +message+ "</p>";		
		_html += "<div class='blk pop-up-btns clearfix'>";
		_html += "<input type='button' value='OK' class='green-btn'>";
		_html += "</div><div class='clearfix border-design'></div></div></div>";
		$("body").append(_html);

		$("#app_alert input[type='button']").unbind("click").bind("click", function(){
			$("#app_alert").remove();
			if(callback) callback();
		});
	}
	// Pop Confirm
	app.lib.confirm=function(message, callback){
		var _html = "<div id='app_confirm' class='clearfix pop-up-container v-mid'>";
		_html += "<div class='pop-alert pop-up-alert'>";
		_html += "<span class='iconStat icon-help'></span>";
		_html += "<p>" +message+ "</p>";
		_html += "<div class='blk pop-up-btns clearfix'>";
		_html += "<input type='button' data-val='1' value='Yes' class='green-btn' style='text-transform: none;' />";
		_html += "<input type='button' data-val='0' value='No' class='red-btn' style='text-transform: none;' />";
		_html += "</div><div class='clearfix border-design'></div></div></div>";
		$("body").append(_html);

		$("#app_confirm input[type='button']").unbind("click").bind("click", function(){
			$("#app_confirm").remove();
			var rs = $(this).attr("data-val") == 1 ? true : false;
			if(callback) callback(rs);
		});
	}
	// Pop Window
	app.lib.popup=function(prop, callback){
		app.lib.popup_hide();
		var prop = prop || {};
		prop.text = prop.text || "";
		prop.width = prop.width || "auto";
		prop.type = prop.type || null;
		var icon;
		switch(prop.type){
			case "success":
				icon = "icon-ok-2";
				break;
			case "error":
				icon = "icon-cancel-2"
				break;
			case "warning":
				icon = "icon-attention-alt";
				break;
			default:
				icon = "";
		}		

		var _html = "<div id='app_popup' class='custom-pop-up-alert clearfix pop-up-container v-mid'>";
		_html += "<div class='pop-alert pop-up-alert' style='width: " +prop.width+ "'>";
		_html += prop.type != null ? "<span class='iconStat " +icon+ "'></span>" : "";
		_html += "<div id='popup_content'>" +prop.text+ "</div>";		
		_html += "<div class='clearfix border-design'></div></div></div>";
		$("body").append(_html);
		if(callback) callback();
	}
	app.lib.popup_hide=function(){
		$("#app_popup").remove();
	}
	// Loader Show/Hide
	app.lib.loader={};
	app.lib.loader.show=function(targetId){
		app.lib.loader.hide();
		var id = targetId || "body";
		var _html = "<div id='app_loader' class='loader v-mid clearfix'><div class='loading'></div></div>";
		$(id).append(_html);
	}
	app.lib.loader.hide=function(){
		$("#app_loader").remove();
	}
	window.app=app;
	window.tmp={};

	// Serialize Form Object
	app.lib.serializeObject = function(form){
        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value.toUpperCase();
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each(form.serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };

})(window, document, $);
